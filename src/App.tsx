import React, { FC, useState } from 'react'
import List from './list'
import Details from './details'
import { Expense, UUID } from './models'

const App: FC<{ data: Expense[] }> = ({ data }) => {
	const [selected, setSelected] = useState<UUID>()
	const [expenses, setExpenses] = useState<Expense[]>(data)

	const handleStatusChange = (id: UUID) => {
		const updated =
			expenses &&
			expenses.map(e => {
				if (e.id === id) {
					return { ...e, paid: !e.paid }
				} else {
					return e
				}
			})

		setExpenses(updated)
	}

	const selectedExpense = expenses && expenses.find(e => e.id === selected)

	return (
		data && (
			<div>
				<div className="container-fluid">
					<div className="row">
						<div className="col-md-8">
							<List
								expenses={expenses}
								onSelect={setSelected}
								onStatusFlip={handleStatusChange}
							/>
						</div>
						<div className="col-md-4">
							{selectedExpense && <Details expense={selectedExpense} />}
						</div>
					</div>
				</div>
			</div>
		)
	)
}

export default App
