import { Expense } from './models'

import React, { FC } from 'react'

type Props = {
	expense: Expense
}

const Details: FC<Props> = ({ expense }) => {
	return (
		<div className="card expenses-details">
			<div className="card-body">
				<h5 className="card-title">Expense details</h5>
				<div className="row">
					<div className="col-md-6">Category</div>
					<div className="col-md-6">{expense.category}</div>
				</div>
				<div className="row">
					<div className="col-md-6">Date</div>
					<div className="col-md-6">{expense.date}</div>
				</div>
				<div className="row">
					<div className="col-md-6">Amount</div>
					<div className="col-md-6">{expense.amount.usd}</div>
				</div>
				<div className="row">
					<div className="col-md-6">Description</div>
					<div className="col-md-6">{expense.description}</div>
				</div>
				<div className="row">
					<div className="col-md-6">Status</div>
					<div className="col-md-6">{expense.paid ? 'PAID' : 'PENDING'}</div>
				</div>
			</div>
		</div>
	)
}

export default Details
