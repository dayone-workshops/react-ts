import React, { FC, useEffect, useState } from 'react'
import ReactDOM from 'react-dom'
import './normalize.css'
import './index.css'
import App from './App'
import fetchExpenses from './fetch-expenses'
import { Expense } from './models'

type Props = {
	children: (data: any) => JSX.Element
}

const FetchData = ({ children }: Props) => {
	const [data, setData] = useState<Expense>()

	useEffect(() => {
		fetchExpenses().then(value => setData(value))
	}, [])

	return data ? children(data) : <div>Loading...</div>
}

ReactDOM.render(
	<React.StrictMode>
		<FetchData>{data => <App data={data} />}</FetchData>
	</React.StrictMode>,
	document.getElementById('root')
)
