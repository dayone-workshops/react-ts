import { sortBy } from 'lodash'
import React, { useMemo, FC, useState } from 'react'
import { Expense, UUID } from './models'
import { calculateTotal } from './models'

type Props = {
	expenses: Expense[]
	onStatusFlip: (uuid: UUID) => void
	onSelect: (uuid: UUID) => void
}

const List: FC<Props> = ({ expenses, onStatusFlip, onSelect }) => {
	const [sortColumn, setSortColumn] = useState('category')
	const total = useMemo(() => calculateTotal(expenses), [expenses])

	const sortedExpenses = sortBy(expenses, sortColumn)

	return (
		<div>
			<table className="table table-hover">
				<thead>
					<tr>
						<th onClick={() => setSortColumn('category')}>Category</th>
						<th onClick={() => setSortColumn('description')}>Description</th>
						<th onClick={() => setSortColumn('date')}>Date</th>
						<th onClick={() => setSortColumn('amount.value')}>Amount</th>
						<th onClick={() => setSortColumn('paid')}>Status</th>
					</tr>
				</thead>
				<tbody>
					{sortedExpenses.map(e => (
						<tr key={e.id} onClick={() => onSelect(e.id)}>
							<td>{e.category}</td>
							<td>{e.description}</td>
							<td>{e.date}</td>
							<td>{e.amount.usd}</td>
							<td onClick={() => onStatusFlip(e.id)}>
								{e.paid ? 'PAID' : 'PENDING'}
							</td>
						</tr>
					))}
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>{total}</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default List
