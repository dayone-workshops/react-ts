export const calculateTotal = (expenses: Expense[]): number =>
	expenses.reduce((acc, e) => acc + e.amount.usd, 0)

export type UUID = string

export type Expense = {
	id: UUID
	category: 'Food' | 'Home' | 'Leisure'
	description: string
	date: string
	amount: {
		usd: number
		eur: number
		pln: number
		yen: number
	}
	paid: boolean
}
